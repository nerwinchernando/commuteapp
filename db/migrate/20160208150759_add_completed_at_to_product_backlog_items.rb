class AddCompletedAtToProductBacklogItems < ActiveRecord::Migration
  def change
    add_column :product_backlog_items, :completed_at, :datetime
  end
end
