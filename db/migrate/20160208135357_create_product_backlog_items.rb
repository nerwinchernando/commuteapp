class CreateProductBacklogItems < ActiveRecord::Migration
  def change
    create_table :product_backlog_items do |t|
      t.text :item
      t.integer :priority
      t.integer :initialsizeestimate

      t.timestamps null: false
    end
  end
end
