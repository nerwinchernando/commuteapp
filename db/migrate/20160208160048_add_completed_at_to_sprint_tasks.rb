class AddCompletedAtToSprintTasks < ActiveRecord::Migration
  def change
    add_column :sprint_tasks, :completed_at, :datetime
  end
end
