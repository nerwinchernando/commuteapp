class CreateSprintTasks < ActiveRecord::Migration
  def change
    create_table :sprint_tasks do |t|
      t.string :content
      t.integer :initialsizeestimate
      t.date :date_completed
      t.references :product_backlog_item, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
