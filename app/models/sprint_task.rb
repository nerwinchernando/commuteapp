class SprintTask < ActiveRecord::Base
  belongs_to :product_backlog_item

  def completed?
  	!completed_at.blank?
  end
end
