class SprintTasksController < ApplicationController
  before_action :set_product_backlog_item
	before_action :set_sprint_task, except: [:create]

	def create
    @sprint_task = @product_backlog_item.sprint_tasks.create(sprint_task_params)
    redirect_to @product_backlog_item
	end

	def destroy
		if @sprint_task.destroy
			flash[:success] = "Sprint task was deleted."
		else
			flash[:error] = "Sprint task could not be deleted."
		end
    redirect_to @product_backlog_item
	end

	def complete
		@sprint_task.update_attribute(:completed_at, Time.now)
    redirect_to @product_backlog_item, notice: "Product backlog item completed"
	end

	private

	def set_product_backlog_item
    @product_backlog_item = ProductBacklogItem.find(params[:product_backlog_item_id])
	end

	def set_sprint_task
		@sprint_task = @product_backlog_item.sprint_tasks.find(params[:id])
	end

	def sprint_task_params
    params[:sprint_task].permit(:content)
	end

end
