class ProductBacklogItemsController < ApplicationController
  before_action :set_product_backlog_item, only: [:show, :edit, :update, :destroy]

  # GET /product_backlog_items
  # GET /product_backlog_items.json
  def index
    @product_backlog_items = ProductBacklogItem.all
  end

  # GET /product_backlog_items/1
  # GET /product_backlog_items/1.json
  def show
  end

  # GET /product_backlog_items/new
  def new
    @product_backlog_item = ProductBacklogItem.new
  end

  # GET /product_backlog_items/1/edit
  def edit
  end

  # POST /product_backlog_items
  # POST /product_backlog_items.json
  def create
    @product_backlog_item = ProductBacklogItem.new(product_backlog_item_params)

    respond_to do |format|
      if @product_backlog_item.save
        format.html { redirect_to @product_backlog_item, notice: 'Product backlog item was successfully created.' }
        format.json { render :show, status: :created, location: @product_backlog_item }
      else
        format.html { render :new }
        format.json { render json: @product_backlog_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /product_backlog_items/1
  # PATCH/PUT /product_backlog_items/1.json
  def update
    respond_to do |format|
      if @product_backlog_item.update(product_backlog_item_params)
        format.html { redirect_to @product_backlog_item, notice: 'Product backlog item was successfully updated.' }
        format.json { render :show, status: :ok, location: @product_backlog_item }
      else
        format.html { render :edit }
        format.json { render json: @product_backlog_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /product_backlog_items/1
  # DELETE /product_backlog_items/1.json
  def destroy
    @product_backlog_item.destroy
    respond_to do |format|
      format.html { redirect_to product_backlog_items_url, notice: 'Product backlog item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product_backlog_item
      @product_backlog_item = ProductBacklogItem.find(params[:id])
    end

    def set_sprint_task
		  @sprint_task = @product_backlog_item.sprint_tasks.find(params[:id])
	  end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_backlog_item_params
      params.require(:product_backlog_item).permit(:item, :priority, :initialsizeestimate)
    end
end
