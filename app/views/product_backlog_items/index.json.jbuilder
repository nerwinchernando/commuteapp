json.array!(@product_backlog_items) do |product_backlog_item|
  json.extract! product_backlog_item, :id, :item, :priority, :initialsizeestimate
  json.url product_backlog_item_url(product_backlog_item, format: :json)
end
