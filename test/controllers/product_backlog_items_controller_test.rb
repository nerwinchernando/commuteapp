require 'test_helper'

class ProductBacklogItemsControllerTest < ActionController::TestCase
  setup do
    @product_backlog_item = product_backlog_items(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:product_backlog_items)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create product_backlog_item" do
    assert_difference('ProductBacklogItem.count') do
      post :create, product_backlog_item: { initialsizeestimate: @product_backlog_item.initialsizeestimate, item: @product_backlog_item.item, priority: @product_backlog_item.priority }
    end

    assert_redirected_to product_backlog_item_path(assigns(:product_backlog_item))
  end

  test "should show product_backlog_item" do
    get :show, id: @product_backlog_item
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @product_backlog_item
    assert_response :success
  end

  test "should update product_backlog_item" do
    patch :update, id: @product_backlog_item, product_backlog_item: { initialsizeestimate: @product_backlog_item.initialsizeestimate, item: @product_backlog_item.item, priority: @product_backlog_item.priority }
    assert_redirected_to product_backlog_item_path(assigns(:product_backlog_item))
  end

  test "should destroy product_backlog_item" do
    assert_difference('ProductBacklogItem.count', -1) do
      delete :destroy, id: @product_backlog_item
    end

    assert_redirected_to product_backlog_items_path
  end
end
